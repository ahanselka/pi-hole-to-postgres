-- Create TimescaleDB extension

CREATE EXTENSION IF NOT EXISTS timescaledb;


COMMENT ON EXTENSION timescaledb IS 'Enables scalable inserts and complex queries for time-series data';

-- Create query table for query data from pi-hole
-- Note: The `id` column is a UNIQUE value in the pihole
-- SQLite DB. However, it cannot be if we want to use
-- with TimescaleDB (as far as I understand).
-- As such, we leave it not UNIQUE here. 
CREATE TABLE query (
    id bigint NOT NULL,
    ts timestamp with time zone NOT NULL,
    query_type_id integer NOT NULL,
    status_id integer NOT NULL,
    domain text NOT NULL,
    client text NOT NULL,
    forward text,
    additional_info text
);

-- Creates the TimescaleDB hypertable.
SELECT create_hypertable('query', 'ts');


-- Create network (which... isn't really networks) table to be filled by pihole
CREATE TABLE network (
    id bigint NOT NULL UNIQUE,
    hwaddr text NOT NULL UNIQUE,
    interface text NOT NULL,
    first_seen timestamp with time zone NOT NULL,
    last_query timestamp with time zone NOT NULL,
    numqueries integer NOT NULL,
    macvendor text,
    aliasclient_id integer
);

-- Create network addresses that links to networks
CREATE TABLE network_addresses (
    network_id BIGINT NOT NULL,
    ip TEXT NOT NULL UNIQUE,
    last_seen TIMESTAMPTZ NOT NULL,
    name TEXT NULL,
    name_updated TIMESTAMPTZ NULL,
    CONSTRAINT fk_network
      FOREIGN KEY(network_id) 
	      REFERENCES network(id)
);

-- Create table mapping query_id to query type.
CREATE TABLE query_type (
    id bigint NOT NULL PRIMARY KEY,
    query_type text NOT NULL
);
COMMENT ON TABLE query_type IS 'Describes the type of DNS query.';

INSERT INTO query_type (id, query_type)
    VALUES (1, 'A'), (2, 'AAAA'), (3, 'ANY'),
        (4, 'SRV'), (5, 'SOA'), (6, 'PTR'),
        (7, 'TXT'), (8, 'NAPTR'), (9, 'MX'),
        (10, 'DS'), (11, 'RRSIG'), (12, 'DNSKEY'),
        (13, 'NS'), (14, 'OTHER'), (15, 'SVCDB'), 
        (16, 'HTTPS')
        ;


-- Create table mapping status_id to whether the query was blocked or not. 
CREATE TABLE status (
    id integer NOT NULL,
    status text NOT NULL,
    allowed boolean NOT NULL,
    description text NOT NULL
);
COMMENT ON TABLE status IS 'Describes the status of the DNS query.';

INSERT INTO status (id, status, allowed, description)
    VALUES (0, 'Unknown', False, 'Unknown status (not yet known)'),
        (1, 'Blocked', False, 'Domain contained in gravity database'),
        (2, 'Allowed', True, 'Forwarded'),
        (3, 'Allowed', True, 'Known, replied to from cache'),
        (4, 'Blocked', False, 'Domain matched by a regex blacklist filter'),
        (5, 'Blocked', False, 'Domain contained in exact blacklist'),
        (6, 'Blocked', False, 'By upstream server (known blocking page IP address)'),
        (7, 'Blocked', False, 'By upstream server (0.0.0.0 or ::)'),
        (8, 'Blocked', False, 'By upstream server (NXDOMAIN with RA bit unset)'),
        (9, 'Blocked', False, 'Domain contained in gravity database. Blocked during deep CNAME inspection'),
        (10, 'Blocked', False, 'Domain matched by a regex blacklist filter. Blocked during deep CNAME inspection'),
        (11, 'Blocked', False, 'Domain contained in exact blacklist. Blocked during deep CNAME inspection'),
        (12, 'Allowed', True, 'Retried query'),
        (13, 'Allowed', True, 'Retried but ignored query (this may happen during ongoing DNSSEC validation)'),
        (14, 'Allowed', True, 'Already forwarded, not forwarding again')
    ;