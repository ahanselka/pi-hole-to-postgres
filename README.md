# Pi-hole DNS Pipeline

This is the setup I use to move data from the built-in [pi-hole](http://pi-hole.net) SQLite database into PostgreSQL and then visualize that in Grafana.

I initially followed Ryan Lambert's [blog post](https://blog.rustprooflabs.com/2021/02/postgresql-sqlite-fdw-pihole) on [RustProof Labs Blog](https://blog.rustprooflabs.com/) to use Postgres foreign data wrappers to do this.
It worked wonderfully! Yet, I wanted more. I really wanted to keep Postgres as up to date with Pihole as possible.
This way, I could use Grafana to visualize fairly "real-time" data. Thus, I wrote this script to do so via cron.
If this is your first time, you should really read the [initial setup](initial_setup.md) page.
I have adapted Ryan's instructions to prepare the database for using TimescaleDB and this script.

## Requirements

- [PostgreSQL](http://postgresql.org) 12 or 13. I recommend using the PGDG repo on [Debian/Ubuntu](https://wiki.postgresql.org/wiki/Apt) or [RHEL/CentOS](https://yum.postgresql.org/repopackages/).
- [TimescaleDB](https://www.timescale.com)
    - [Docs for Self-hosted TimescaleDB](https://docs.timescale.com/timescaledb/latest/how-to-guides/install-timescaledb/self-hosted/)
- [Grafana Install](http://grafana.com)
- Ruby with Sequel extension on the pihole server
    - On Ubuntu `apt install ruby ruby-sequel ruby-sequel-pg` should be sufficient.

## PostgreSQL Sync Setup Steps

The following section assumes that you have loaded initial data into PostgreSQL. If you haven't, there are instructions in the [initial setup doc](initial_setup.md).

1. Create a user on the postgres server that the pihole can use to connect. It needs to at minimum have the following permissions.
    - `INSERT`, `SELECT` on `query`
    - `INSERT`, `SELECT`, `UPDATE` on `network`
    - `INSERT`, `SELECT`, `UPDATE` on `network_addresses`
1. On the pihole server, get a shell as the pihole user. The login shell is set to `nologin` by default, so I just used `sudo -u pihole /bin/bash` instead of changing it.
1. Copy the [`pihole-to-pg`](pihole-to-pg.rb) script to the pihole user's homedir (default `/home/pihole`).
1. Update line 4 to include the actual values for your pg user, password, and database.
1. Add the script to a cron job running as often as you wish. You should run it no more frequently than you have [`DBINTERVAL`](https://docs.pi-hole.net/ftldns/configfile/#dbinterval) set.

## Grafana Setup

1. Create a Postgres user for Grafana to connect to. Be sure it has only `SELECT` privileges on your db!
```sql
CREATE USER grafana_data WITH PASSWORD 'whatever';
GRANT SELECT ON query,network_addresses,network,status,query_type TO grafana_data;
```
1. Add Postgres as a datasource on Grafana.
    - You can follow [the grafana docs](https://grafana.com/docs/grafana/latest/datasources/postgres/) on this.
    - Be sure to select that this is a TimescaleDB. The dashboard assumes your datasource will be named "Pi-hole PostgreSQL".
1. Import the dashboards that are in the grafana directory by going to the + and clicking import. You can paste the JSON right on in there.