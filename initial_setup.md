# Initial SQLite to PostgreSQL sync

The initial sync from the SQLite DB to Postgres is going to be huge depending on the amount of data you have.
The script here is not very efficient and doesn't scale to large amounts of updates at once.
If you don't care about efficiency, feel free to run the script in `tmux` and let it go for a long while.

Otherwise, please follow the below instructions to use postgres foreign data wrappers adapted from Ryan Lambert's [blog post](https://blog.rustprooflabs.com/2021/02/postgresql-sqlite-fdw-pihole) on [RustProof Labs Blog](https://blog.rustprooflabs.com/).
I don't go into as much detail below and focus primarily on the commands to run to start syncing. I really recommend you check out his post if you have time as it was super informative and worth a read!
I think the primary difference in the docs is that I tend to just use the public schema and Ryan prefers to use named schema instead.
I am of the opinion that for my uses, it is just as well for me to use the public schema and all the other instructions in here assume you're using the public schema (or the schema you're using is first in the `search_path`).

Additionally, I am including info on what you need to make TimescaleDB work. You should go make sure it is [installed and ready to go]((https://docs.timescale.com/timescaledb/latest/how-to-guides/install-timescaledb/self-hosted/) before you continue.

## Initial Setup

### Backup SQLite DB

Firstly, backup the SQLite database to a separate file and move it to your database server or wherever Postgres is running.

```
sqlite3 /etc/pihole/pihole-FTL.db ".backup /tmp/pihole-FTL-backup.db"
```

Going forward, I will assume that you've put the backup on the postgres server at `/tmp/pihole-FTL-backup.db`.

### Setup FDW and connect to the Pi-hole DB

The FDW for SQLite does not ship with Postgres, but is available in [a GitHub repo](https://github.com/pgspider/sqlite_fdw). Follow the instructions there to install the extension.
You may need to install some additional packages in order to build it.
Note: You will need to run the `make install` step with `sudo`.

Create database and connect to it.

```bash
psql -c "CREATE DATABASE pihole;"
psql -d pihole
```

Create the extension and point it to your pihole db file.

```sql
CREATE EXTENSION sqlite_fdw;
CREATE SERVER pihole_backup_server
    FOREIGN DATA WRAPPER sqlite_fdw
    OPTIONS (DATABASE '/tmp/pihole-FTL-backup.db')
;
```

Create the foreign tables in which to get the initial data. The [Pi-hole docs](https://docs.pi-hole.net/database/ftl/#query-table) describes the schema if you're curious. 

```sql
CREATE FOREIGN TABLE query_fdw
(
    id BIGINT NOT NULL,
    timestamp BIGINT NOT NULL,
    type INT NOT NULL,
    status INT NOT NULL,
    domain TEXT NOT NULL,
    client TEXT NOT NULL,
    forward TEXT NULL,
    additional_info TEXT NULL
)
SERVER pihole_backup_server
OPTIONS (TABLE 'queries')
;
```

You can check to see if it works:

```sql
SELECT * FROM query_fdw LIMIT 1;
```

### Create Postgres and TimescaleDB tables

First up, we are going to create the postgres table that will house the data and create a TimescaleDB hypertable on it.
In Ryan's example he made the `id` column into the `PRIMARY KEY`. That didn't work for me with TimescaleDB so I left out a primary key on the query table.

```sql
CREATE EXTENSION IF NOT EXISTS timescaledb;
COMMENT ON EXTENSION timescaledb IS 'Enables scalable inserts and complex queries for time-series data';

CREATE TABLE query (
    id bigint NOT NULL,
    ts timestamp with time zone NOT NULL,
    query_type_id integer NOT NULL,
    status_id integer NOT NULL,
    domain text NOT NULL,
    client text NOT NULL,
    forward text,
    additional_info text
);

-- Creates the TimescaleDB hypertable.
SELECT create_hypertable('query', 'ts');
```

Load the initial data! The `timestamp` field in the Pi-hole DB is a UNIX timestamp, so we are converting it here to a `TIMESTAMPTZ`. This step should be fairly fast.

```sql
INSERT INTO query
    (id, ts, query_type_id, status_id, domain, client, forward, additional_info)   
SELECT id, TO_TIMESTAMP(timestamp) AS ts, type AS query_type_id,
        status AS status_id, domain, client, forward, additional_info
    FROM query_fdw
;
```

Create some indexes as below. These are some I made using the examples in the [TimescaleDB documentation on indexes](https://docs.timescale.com/timescaledb/latest/how-to-guides/schema-management/indexing/).
I have no idea if all of them help or not, but they seem to for me *shrug*.

```sql
CREATE INDEX ON query (client, ts DESC);
CREATE INDEX ON query (forward, ts DESC);
CREATE INDEX ON query (query_type_id, ts DESC);
CREATE INDEX ON query (status_id, ts DESC);
```

### Create supporting tables

There are several tables that include supporting data, such as a mapping for `query_type_id` and `status_id`
as well as two other tables from the Pi-hole database for clients and IPs.
These don't need seeding as they are generally small and the script can handle it fine.

```sql
-- Create network (which... isn't really networks) table to be filled by pihole
CREATE TABLE network (
    id bigint NOT NULL UNIQUE,
    hwaddr text NOT NULL UNIQUE,
    interface text NOT NULL,
    first_seen timestamp with time zone NOT NULL,
    last_query timestamp with time zone NOT NULL,
    numqueries integer NOT NULL,
    macvendor text,
    aliasclient_id integer
);

-- Create network addresses that links to networks
CREATE TABLE network_addresses (
    network_id BIGINT NOT NULL,
    ip TEXT NOT NULL UNIQUE,
    last_seen TIMESTAMPTZ NOT NULL,
    name TEXT NULL,
    name_updated TIMESTAMPTZ NULL,
    CONSTRAINT fk_network
      FOREIGN KEY(network_id) 
	      REFERENCES network(id)
);

-- Create table mapping query_id to query type.
CREATE TABLE query_type (
    id bigint NOT NULL PRIMARY KEY,
    query_type text NOT NULL
);
COMMENT ON TABLE query_type IS 'Describes the type of DNS query.';

INSERT INTO query_type (id, query_type)
    VALUES (1, 'A'), (2, 'AAAA'), (3, 'ANY'),
        (4, 'SRV'), (5, 'SOA'), (6, 'PTR'),
        (7, 'TXT'), (8, 'NAPTR'), (9, 'MX'),
        (10, 'DS'), (11, 'RRSIG'), (12, 'DNSKEY'),
        (13, 'NS'), (14, 'OTHER'), (15, 'SVCDB'), 
        (16, 'HTTPS')
        ;


-- Create table mapping status_id to whether the query was blocked or not. 
CREATE TABLE status (
    id integer NOT NULL,
    status text NOT NULL,
    allowed boolean NOT NULL,
    description text NOT NULL
);
COMMENT ON TABLE status IS 'Describes the status of the DNS query.';

INSERT INTO status (id, status, allowed, description)
    VALUES (0, 'Unknown', False, 'Unknown status (not yet known)'),
        (1, 'Blocked', False, 'Domain contained in gravity database'),
        (2, 'Allowed', True, 'Forwarded'),
        (3, 'Allowed', True, 'Known, replied to from cache'),
        (4, 'Blocked', False, 'Domain matched by a regex blacklist filter'),
        (5, 'Blocked', False, 'Domain contained in exact blacklist'),
        (6, 'Blocked', False, 'By upstream server (known blocking page IP address)'),
        (7, 'Blocked', False, 'By upstream server (0.0.0.0 or ::)'),
        (8, 'Blocked', False, 'By upstream server (NXDOMAIN with RA bit unset)'),
        (9, 'Blocked', False, 'Domain contained in gravity database. Blocked during deep CNAME inspection'),
        (10, 'Blocked', False, 'Domain matched by a regex blacklist filter. Blocked during deep CNAME inspection'),
        (11, 'Blocked', False, 'Domain contained in exact blacklist. Blocked during deep CNAME inspection'),
        (12, 'Allowed', True, 'Retried query'),
        (13, 'Allowed', True, 'Retried but ignored query (this may happen during ongoing DNSSEC validation)'),
        (14, 'Allowed', True, 'Already forwarded, not forwarding again')
    ;
```

Alrighty, head back to the [README.md](README.md) to start with the setup for the script!