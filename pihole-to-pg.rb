require 'sequel'

# Connect to both source (pihole) and destination (postgres) databases
dest = Sequel.connect('postgres://PIHOLE_DB_USER:PIHOLE_DB_PASSWORD@POSTGRES_IP_ADDRESS/PIHOLE_DB')
src = Sequel.sqlite('/etc/pihole/pihole-FTL.db')

# Set the most recent ID that has been synced to postgres.
# This will return a NoMethodError if there is no data in the table, 
# if it does, you probably don't want to run this script to do the initial sync.
begin
  destid = dest[:query].select(:id).reverse(:id).first[:id]
rescue NoMethodError
  abort(
    'You have no data in Postgres and this script will take *obscenely* long to run to do an initial sync. 
If you really want to do so anyway, replace these lines (13-16)) with `destid = 0` and re-run.'
    )
end

# Set the query that will get all data from the pihole `queries` table that is greater than the latest ID in postgres
srcdata = src[:queries].where{id > destid}.order_by(:id)

# Set the query for the `network` table
srcnetworks = src[:network]
destnetworks = dest[:network]

# Set the query for the `network_addresses` table
srcaddresses = src[:network_addresses]
destaddresses = dest[:network_addresses]

updatecount = 0

# Iterate through each row of the source data
# INSERTing it into the Postgres database. This is not terribly efficient
# and could take a long time if you've got a lot of rows. Luckily,
# if you run this fairly regularly, you won't have a large amount of rows to sync.
srcdata.each do |row|
  ts = Time.at(row[:timestamp])
  dest[:query].insert(
    id: row[:id],
    ts: ts,
    query_type_id: row[:type],
    status_id: row[:status],
    domain: row[:domain],
    client: row[:client],
    forward: row[:forward],
    additional_info: row[:additional_info]
  )
  updatecount += 1
end


# Itterate through each entry in the `network` table.
# This table includes a list of "networks" that pihole knows about and various
# information about them. I would call these "clients" myself.
# There are unique constraints on the `id` and `hwaddr` columns.
# As such, this does an "upsert" and updates the important columns when
# the `id` column conflicts. Otherwise it just adds a new row.
srcnetworks.each do |row|
  firstts = Time.at(row[:firstSeen])
  lastts = Time.at(row[:lastQuery])
  destnetworks.
    insert_conflict(
    target: :id, update: {last_query: lastts, numqueries: row[:numQueries]}, 
    update_where: {Sequel[:network][:id] => row[:id]}).
    insert(
    id: row[:id],
    hwaddr: row[:hwaddr],
    interface: row[:interface],
    first_seen: firstts,
    last_query: lastts,
    numqueries: row[:numQueries],
    macvendor: row[:macVendor],
    aliasclient_id: row[:aliasclient_id]
  )
end

# Start a transaction and iterate over all the rows in `network_addresses`.
# This table links IP addresses to the networks defined in the `network` table.
# The unique column here is the IP address, so on conflict we update relevant
# columns for that IP address instead. Otherwise insert a new row.
srcaddresses.each do |row|
  lastseen = Time.at(row[:lastSeen])
  nameupdated = Time.at(row[:nameUpdated]) unless row[:nameUpdated] == nil
  destaddresses.
  insert_conflict(target: :ip, update: {last_seen: lastseen, name_updated: nameupdated, name: row[:name]},
    update_where: {Sequel[:network_addresses][:ip] => row[:ip]}).
  insert(
      network_id: row[:network_id],
      ip: row[:ip],
      last_seen: lastseen,
      name: row[:name],
      name_updated: nameupdated
  )
end


# This outputs how many rows in the query table were updated. Surprised?
puts "#{updatecount} rows were updated in the query table."
